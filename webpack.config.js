const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const CopyPlugin = require("copy-webpack-plugin");

module.exports = {
  entry: "./src/index.js",
  devtool: 'inline-source-map',
  devServer: {
    static: './dist',
    watchFiles: ['./src/index.html']
  },
  plugins: [
    new HtmlWebpackPlugin({
      title: "Development",
      template: "./src/index.html"
    }),
    new CopyPlugin({
      patterns: [
        { from: "src/assets/logo/favicon.png", to: "logo/favicon.png" },
      ],
    }),
  ],
  output: {
    filename: "[name].bundle.js",
    path: path.resolve(__dirname, "dist"),
    clean: true,
  },
  module: {
    rules: [
      {
        test: /\.css$/i,
        use: ["style-loader", "css-loader"],
      },
      {
        test: /\.(png|svg|jpg|jpeg|gif)$/i,
        type: "public/logo",
      },
      {
        test: /\.(woff|woff2|eot|ttf|otf)$/i,
        type: 'public',
      },
    ],
  },
  optimization: {
    runtimeChunk: 'single',
  },
};