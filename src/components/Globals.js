export const { PI, hypot, sqrt, sin, cos, round } = Math;
window.PI = PI;
window.hypot = hypot;
window.sqrt = sqrt;
window.sin = sin;
window.cos = cos;
window.round = round;

window.PEN_COLOR = Object.freeze({
  BLACK: "#000000",
  RED: "#FF0000",
  GREEN: "#00FF00",
  BLUE: "#0000FF",
  ERASE: "#FFF8DC",
});


window.whiteboard;