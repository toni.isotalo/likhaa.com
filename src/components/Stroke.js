export default class Stroke {
  constructor(
    points = [],
    color = PEN_COLOR.BLACK,
    pen_size = 3,
    pen_sharpeness = 2,
    isLine = false,
    image = null
  ) {
    this.points = points;
    this.color = color;
    this.pen_size = pen_size;
    this.pen_sharpeness = pen_sharpeness;
    this.is_line = isLine;
    this.image = image;
  }

  static DEFAULT_PEN_SIZE = 3;
  static DEFAULT_ERASER_SIZE = 32;
  static DEFAULT_PEN_SHARPNESS = 2;
}