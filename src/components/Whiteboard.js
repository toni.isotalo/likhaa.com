import Stroke from "./Stroke";

export default class Whiteboard {
  constructor(
    parent = document.body,
    color = PEN_COLOR.ERASE,
    is_pen_down = false
  ) {
    this.parent = parent;
    this.create_canvas();

    this.color = color;
    this.is_pen_down = is_pen_down;
    this.strokes_array = [];
    this.undo_strokes_array = [];

    this.current_pen_color = 0;
    this.current_pen_size = Stroke.DEFAULT_PEN_SIZE;
    this.current_eraser_size = Stroke.DEFAULT_ERASER_SIZE;
  }

  create_canvas = () => {
    this.board = document.createElement("CANVAS");
    this.board.setAttribute("id", "whiteboard");
    this.ctx = this.board.getContext("2d");

    // Set the Pen color to currently selected color
    this.ctx.fillStyle =
      Whiteboard.supported_pen_colors[this.current_pen_color];

    this.board.style.width = this.board.width = window.innerWidth;
    this.board.style.height = this.board.height = window.innerHeight;
    this.board.style.cursor = Whiteboard.pen_cursor_black;

    this.parent.appendChild(this.board);

    this.setup_image_drag_and_drop();
  };

  setup_image_drag_and_drop = () => {
    // Provide Image drop support
    this.board.ondragenter = function (evt) {
      evt.stopPropagation();
      evt.preventDefault();
      // console.log("DRAGENTER");
    };
    this.board.ondragleave = function (evt) {
      // evt.preventDefault();
      // console.log("DRAGLEAVE");
    };

    const ctx = this.ctx;
    const wb = this;

    this.board.ondrop = function drop_handler(ev) {
      // console.log("DROP");
      ev.stopPropagation();
      ev.preventDefault();

      // TODO: Add the image to undo queue as well
      const new_img_stroke = new Stroke(null, null, null, null, null, ev.dataTransfer.files[0]);
      wb.strokes_array.push(new_img_stroke);
      Whiteboard.draw_image(ev.dataTransfer.files[0], ctx);
    };

    this.board.ondragover = function dragover_handler(ev) {
      console.log("DRAGOVER");
      ev.stopPropagation();
      ev.preventDefault();
      // ev.dataTransfer.dropEffect = "move";
    };
  };

  draw_dot(x, y, pen_radius = Stroke.DEFAULT_PEN_SIZE) {
    //console.log(`${x}, ${y}, ${pen_radius}`)

    this.ctx.beginPath();
    this.ctx.arc(x, y, pen_radius, 0, 2 * Math.PI, true);
    this.ctx.fill();
  }

  render_line = (p1, p2, d = Stroke.DEFAULT_PEN_SIZE) => {
    //console.log(`p1.x: ${p1.x}, p1.y: ${p1.y}, p2.x: ${p2.x}, p2.y: ${p2.y}`);

    const D = hypot(p2.x - p1.x, p2.y - p1.y);

    var last_stroke = this.strokes_array[this.strokes_array.length - 1];
    for (let i = 0; i < D; i += Stroke.DEFAULT_PEN_SHARPNESS) {
      let x0, y0;

      const dt = i;
      const t = dt / D;

      if (p1.x == p2.x) x0 = p1.x;
      else x0 = (1 - t) * p1.x + t * p2.x;

      if (p1.y == p2.y) y0 = p1.y;
      else y0 = (1 - t) * p1.y + t * p2.y;

      this.draw_dot(x0, y0, last_stroke.pen_size);

      last_stroke.points.push({ x: x0, y: y0 });
    }
  };

  draw = (e) => {
    // console.log(`which: ${e.which}, is_pen_down: ${whiteboard.is_pen_down}`);
    // console.log(`x: ${e.clientX}, y: ${e.clientY}`);

    if (e.type === "touchmove" && this.is_pen_down) {
      const last_stroke_points =
        this.strokes_array[this.strokes_array.length - 1].points;
      last_stroke_points.push({
        x: e.changedTouches[0].pageX,
        y: e.changedTouches[0].pageY,
      });

      if (last_stroke_points.length > 1) {
        this.render_line(
          last_stroke_points[last_stroke_points.length - 2],
          last_stroke_points[last_stroke_points.length - 1]
        );
      }
    } else if (
      e.type === "mousemove" &&
      this.is_pen_down &&
      e.which === 1 &&
      e.clientX > 0 &&
      e.clientX < window.innerWidth &&
      e.clientY > 0 &&
      e.clientY < window.innerHeight
    ) {
      const last_stroke_point =
        this.strokes_array[this.strokes_array.length - 1].points;
      last_stroke_point.push({ x: e.clientX, y: e.clientY });

      if (last_stroke_point.length > 1) {
        this.render_line(
          last_stroke_point[last_stroke_point.length - 2],
          last_stroke_point[last_stroke_point.length - 1]
        );
      }
    } else if (e.type === "click") {
      let pen_size = this.is_eraser_enabled()
        ? this.current_eraser_size
        : this.current_pen_size;

      this.draw_dot(e.clientX, e.clientY, pen_size);
    } else if (e.type === "contextmenu") {
      var last_stroke =
        this.strokes_array[this.strokes_array.length - 1].points;
      last_stroke.push({ x: e.clientX, y: e.clientY });
      if (last_stroke.length > 2) {
        this.render_line(
          last_stroke[last_stroke.length - 2],
          last_stroke[last_stroke.length - 1]
        );
      }
    }
  };

  clear_white_board = (undo_mode = false) => {
    // Draw the canvas background with default color
    this.ctx.fillStyle = PEN_COLOR.ERASE;
    this.ctx.fillRect(0, 0, window.innerWidth, window.innerHeight);

    // Change the Pen color back to selected color
    this.ctx.fillStyle =
      Whiteboard.supported_pen_colors[this.current_pen_color];

    if (!undo_mode) {
      this.strokes_array = [];
      this.undo_strokes_array = [];
    }
  };

  change_marker_color(pen_color) {
    if (!pen_color) {
      this.current_pen_color =
        (this.current_pen_color + 1) % Whiteboard.supported_pen_colors.length;
      pen_color = Whiteboard.supported_pen_colors[this.current_pen_color];
      this.color = pen_color;
    } else {
      this.current_pen_color =
        Whiteboard.supported_pen_colors.indexOf(pen_color);
      this.color = pen_color;
    }

    this.ctx.fillStyle = pen_color;

    switch (pen_color) {
      case PEN_COLOR.BLACK:
        this.board.style.cursor = `url(${Whiteboard.pen_cursor_black}) 0 16, auto`;
        break;
      case PEN_COLOR.RED:
        this.board.style.cursor = `url(${Whiteboard.pen_cursor_red}) 0 16, auto`;
        break;
      case PEN_COLOR.GREEN:
        this.board.style.cursor = `url(${Whiteboard.pen_cursor_green}) 0 16, auto`;
        break;
      case PEN_COLOR.BLUE:
        this.board.style.cursor = `url(${Whiteboard.pen_cursor_blue}) 0 16, auto`;
        break;
      case PEN_COLOR.ERASE:
        this.board.style.cursor = `url(${Whiteboard.pen_cursor_eraser}) 15 28, auto`;
        break;
      default:
        ctx.fillStyle = PEN_COLOR.BLACK;
        this.board.style.cursor = `url(${Whiteboard.pen_cursor_black}) 0 16, auto`;
        break;
    }
  }

  pen_up = () => {
    // console.log("PEN_UP");
    this.is_pen_down = false;
    // console.log(this.strokes_array);
  };

  pen_down = (event) => {
    if (event.ctrlKey) return;

    // console.log("PEN_DOWN");
    // Create a new stroke
    this.is_pen_down = true;

    let new_stroke;
    let stroke_pen_size = whiteboard.is_eraser_enabled()
      ? whiteboard.current_eraser_size
      : whiteboard.current_pen_size;
    let stroke_pen_color = whiteboard.is_eraser_enabled()
      ? PEN_COLOR.ERASE
      : Whiteboard.supported_pen_colors[this.current_pen_color];

    if (event.type === "touchstart") {
      new_stroke = new Stroke(
        [
          {
            x: event.changedTouches[0].pageX,
            y: event.changedTouches[0].pageY,
          },
        ],
        stroke_pen_color,
        stroke_pen_size
      );
    } else if (event.type === "mousedown") {
      new_stroke = new Stroke(
        [{ x: event.clientX, y: event.clientY }],
        stroke_pen_color,
        stroke_pen_size
      );
    }

    this.strokes_array.push(new_stroke);
  };

  undo_stroke = () => {
    console.log("UNDO LAST STROKE!");
    // console.log(this.strokes_array);
    const last_stroke = this.strokes_array.pop();

    if (last_stroke) this.undo_strokes_array.push(last_stroke);

    this.redraw_strokes();
  };

  redo_stroke = () => {
    console.log("REDO LAST UNDO STROKE!");
    console.log(this.strokes_array);
    const last_undo_stroke = this.undo_strokes_array.pop();

    if (last_undo_stroke) this.strokes_array.push(last_undo_stroke);

    this.redraw_strokes();
  };

  redraw_strokes = () => {
    this.clear_white_board(true);
    // console.log(`redraw_strkoes(): ${this}`);

    for (let i = 0; i < this.strokes_array.length; i++) {
      // draw image strokes
      if(this.strokes_array[i].image) {
        Whiteboard.draw_image(this.strokes_array[i].image, this.ctx);
        continue;
      }

      // draw normal strokes
      const points = this.strokes_array[i].points;
      this.ctx.fillStyle = this.strokes_array[i].color;
      // console.log(this.strokes_array[i].color);
      for (let j = 0; points.length > 0 && j < points.length; j++) {
        this.draw_dot(points[j].x, points[j].y, this.strokes_array[i].pen_size);
      }
    }
  };

  is_eraser_enabled = () => {
    return this.ctx.fillStyle.toLowerCase() === PEN_COLOR.ERASE.toLowerCase();
  };

  increase_pen_size = () => {
    if (this.is_eraser_enabled()) {
      this.current_eraser_size += 1;
    } else {
      this.current_pen_size += 1;
    }
  };

  decrease_pen_size = () => {
    if (this.is_eraser_enabled() && this.current_eraser_size > 1) {
      this.current_eraser_size -= 1;
    } else if (this.current_pen_size > 1) {
      this.current_pen_size -= 1;
    }
  };

  static default_pen_color = PEN_COLOR.BLACK;
  static supported_pen_colors = [
    PEN_COLOR.BLACK,
    PEN_COLOR.RED,
    PEN_COLOR.GREEN,
    PEN_COLOR.BLUE,
  ];

  // Designed on https://www.piskelapp.com/p/create/sprite by Neetish Raj
  static pen_cursor_black =
    "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAAAXNSR0IArs4c6QAAAkNJREFUWEedV90ahSAI0/d/6M6nAm78aJ1uKjUYY6D19vfVW2sPf+2G8HU8j8t90XRcpuWTeUsczFUwzkuGbbbXW+tPdIqoHYCcjo0HAdJaDCyxmQXTW2+PZ+BjPpbdZeURbtbzsE6h66uHQmirPFWwemuPhd7prbXeO8xacmZKXBbJvs1VEtg62L5FGoIG4yhTjERdeE+RkJiTgEZCBiXL9iGYsYpKg97zSV9JGoBFLuK/+bZ5xwB6DdBr5yJIdB6oxbI04eb87YpWdSNRLJQV+agpqyrNsG8+MRelSJwyh3G4CIE2uGjrlHtJvaL3dQziyXrZtJyqPZdNjaRmQHJalMh2vhqaKb1ylY4PTWwJryTumim7uHa5QcFYZJdioVHoFuqBtpMCMRfwXjQBS89z7MXqUSmaY98TkAFHdVZuaZ1/3D1kOWYwt+A3eu1qM0eBtYPasUudosJErh2OU6tj0XlJwQtUeoBY4iF8XPUr6bDFYgXeYgTWAwU9p5P0nwQS5VbRcGfhXSc8bivIX96GrOyyYO4YvTyYUvqeqHEHlEtHE5AODvKTSBjbw1SRDSSqsp63dzdpCwcOdCrTHJ4zMhOlWZmAm2ul1Zaf7ob/9SHeVx1dLqlcoJGKm4Zu5IQqCC7o5yKaqwGIpaKmKbtc16EpQYrwSB1/e0CD6/zkj+ouYWP9hFmWks9w8LAWBBY+5GWnwDiRh+N+GLXnC++GQYEHDQTTRdT+JzUcXxIElSDPPw7yq7cPxbnlDWDN+1UVIz9rBQE3CcffQgAAAABJRU5ErkJggg==";
  static pen_cursor_red =
    "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAAAXNSR0IArs4c6QAAAlJJREFUWEelV1mWxCAIxL7/mYd5CCirzJKPfp1EWYoqMAsWwEIABKC/gPIL++In6Woe54XFE7uXfP/PuotEc7hWi9ijvyIAv6RO9m5bCwER8AsAPozYagEK2RO4xlKN9g/CQU0UF8CHbwTcGYI+2m1lAUYO+PS2c3JMP4QG8UmuN7piZy+S/YFvEch0j6jsFbJ+GER1fDnxYGeOso4mmjiwkzu6+VwJ0Vp2TvjSy4dqTgBnzSSxBei8k3Ar5yHk0j4xRYMsZZ9Za3xzdpLBJp3EllB95TSQkNMQXh3CKdGu8w048bVRlQlB/1oSWto2wkFiOpeTPJHmd5npaXZu4Q/p31tft5qnvGY3GYsr/ZcOTjFQJ2olpzLjJTm9qWHczD1kxy7F4s1q9xioL5tM5JFxu8O6meRqznOL6/GYW9PLLEOGF792zVnKnu2GcE2S8XFedkcwB3ALpQHv5G1y0pkJ783FHuDSXV19lg1Hc7qqU17s63tVS7jL5Ff9WTiaXmmM0Rfd6WyTidPPt4dTpzG/LkEpr6n6PAljuSqND1xw3bZo0e2s2naPcQnplajiec93je1rxCjLnAZnGrfK6+LzLqtDQ7mzPJIk4YeqPo9WFqQjwLYG5kUvrleQs+VCBTWIMfLqkGHoktt+T9Rkeg67bL1nPIXvjLc5j8AkZfMRU5qd3qdNVL4XblNA0eBL8KkZcTesx/GvHPPiX21x5H7u1Jcdtuo8fj/0monzolBBdYr+SWd6Y9BVOgRgb8d+2n89jwcUj0MmtEvmjzKthlYhnW/eFREzmms7QAAAAABJRU5ErkJggg==";
  static pen_cursor_green =
    "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAAAXNSR0IArs4c6QAAAlBJREFUWEelV0lyBCEMg/z/zUMKb1jGBibpQ6p6GrzIkiC9/evprbXx7witQZxvg9r6Wcl8kWfFqSN22WAr3pP78L23MT5D01MR9P0UrrfWbYvteEQTAw+YAmPgkIjBcXMCWV520cygNoe0MofwAzPA6VptOq3hRjA/ej7dptHb6J/eZvswwdV94ITOhRHhnRtU9QhCPQj7DPRDQCiiW+ysn/lbmFUQxMZn+oGSc0D5O6dAP9G7Sy5MOyC7FXChoetcekLSUXLtdsER+nevSzJ39TLhPFF88kUHjuSrEARmZYSPewq5eIAX7GAyU8Qnwh2hXI0c9GqONoqyeeZ9qBCgL07B7TLnVWalD/j+VCQaA4fG1KKw6gAgfkX5pOTIE9Sylmso6xCdy8WZ54pBPoTZA2PDyBi9ZVjqcEo7gv3sVWI2uooIrM/CCDhgsCi2HzlQJK0sZthTQWSH863MnK2AuZThWbTtojRgDW83hcSIKIoVoGyWlll8JpB1EM0PfmJ5X7s1WQFBHMMOC/kAR3dCJqZndJoz/MtIruucaKjV6wasxQrb7bEawdHHUtHDaKqr4uaw+2n4ljlkUwIoptk9tQAtQeBQQiK9mCs7cNKIQTHJGneWoxOnFIidvF6urgh42qgRZB1ZjYCSf8lnIAVcWP2MKx4Ie1RNN+hKXt4Jy416GfmD3uGC4qCsjIh15K90y8RKlt7dYZcJh33ZmazJkXL/I0CpSOouXnslYSUhDWD1H5pIP+0yPDhFvJgoZNuWBygTLwkX2Ae3+d4uyx2/EYsLNOiPRfkAAAAASUVORK5CYII=";
  static pen_cursor_blue =
    "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAAAXNSR0IArs4c6QAAAk9JREFUWEedV1mWBCEI07n/mcd5yBYQl5r+qK7XIksg0e6ttdbp0Vob89lbl7cxVwYbDHkXW9zEy721wR7sY1vYZ1zlRdpGXxKa3pMTyXDayVJKRePRqtYiSWt5tU+trVEafZShxfkmpNc6GD+ym1Dws4gbIcKMAQNEefWxtGK08QvFd8qhx+hVAY760jM3v1TeaTKGx7JZsWpywcuMULowaGf7hIzAHqdOuqkz7TOxcU3jqeOPr6H3BQ5ceVj4Uao8B9cRfSobhmpWHpOC4My3VLnWqRMVmkxdUDRREVAPjE7DbIHjSUqA2pTJqgBKEeEMGFSCs1AtK42JSFH5HVzBRvPUshBglSCFz/iOLdRS14hXIm2kBvaV026wlz2/V46MCgOVMBitochY0XNYRD+vVANdlrxd3ubAKG+SoQTf9Fy196rSNy2qHfyDat5LI1UZO/LC4At5mMoQ/CbtAvtUXoAdOH06kRcgXYA5iBzLEns2aVaiC2zDwW1Q7BdTU3C21F+xvBwgLwQVi7dzZi7fp4AxAxE8Hzi5/NQeXObnPuvJO78eLR8otBZ8olVudXmfgCnteinDm9iuV/lauBnyYh72XHUEks15S75e+nnyVRbsQuKnQTy9bg4ZrccLYAHZjgXV3bim1eOw7cxiAlJuEqXQ0wWRCqIbbJDNlQUffLHbjxvoDil/Ri7qYsv3CCcEcysEgbvTsBHNP24tE3jz8WZVBdj+QRIO1noCJw9f9orL5TMDHoToFdXVFfzyDz1IM3CAWZfyd6EOe6xW/3/ydgw1zN04NQAAAABJRU5ErkJggg==";
  static pen_cursor_eraser =
    "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAAAXNSR0IArs4c6QAAAotJREFUWEeVV1mWwzAIM73/met5bGa3p/mZaWILIQROYD0vWGvtfhWsBRufXtbgzjsEY4c18iPeg7X2QOTQwx147TlowtYdPYOnOmmBoVXRBhX8lh/CPSQPiqhquif+rQToeQ3gxO2IYpQmmRy0GuI3BVoWsMmJXPorXuuzIeF3OTgUR46KRRL/aJCmYg/BGfRY3ccAgLU3MZov1wmXhQabk+CGnOqLZiBhtDBPLnFB6tNjG2NxhgEgCdgyHhxNwCd0XxKc6xAUeAhfqy3UJZz1QZxDUMJ7rn4ESv2mKbYXQp1peOawU9CVTf6VUlmiaRs/GBU6D9jnQkB50GbiNABEwriy+KEQMNCz1sFzoBPuVJgqbvjpkHI8iumDNEYwZe5EIjCbAK6FhcS9+Y5rKBEkOq1XZfcXF5Lf3eSxXVMzphOAAVwF/L6OAyfpjl8kMYbN4DyMdLlWNb43uAblhQKiPvfBmTqQEgdVSsEvJAEiwVWTHzUvJRAFvrTbH3UAH6dlVxhNbdPGscGEZh0SliKT+CIJiUnY6IlP8EToCpaDcuP797eoaEJPl58IiaZ/tRxOBJmIAZMgrbyFTm+QaEsScm9Rwlvx42eLhBrcnouRuqCp1GEtxwp1BXvCX9qiMqdro5ht+YDyvSEeyV3QVu10B3pC5P9gu9luF0r7Jg7fzpAxnQIRa6FfATScUH622YDRiXHOzfA2kaO8frNw8kWSs/Kmc+uaSdFId48cztHmLdjbapi5TUdW+VxKuSvxHNSq65lY68p37gOIRwVitfWzSiVNXqhNv9uWPrN/mDBn1L0JTcWLjMP7iWiUmnr6EE5A3bJigarjD204ZNRIrBMugMdU3fBS75ob/wD1aDs32JEreQAAAABJRU5ErkJggg==";

  static draw_image(image_file, ctx) {
    const reader = new FileReader();
    reader.onload = function (event) {
      const img = new Image();
      img.onload = function () {
        const PADDING_LEFT = 100;
        const PADDING_RIGHT = 50;
        const PADDING_TOP = 50;
        const PADDING_BOTTOM = 50;
        let pos_x = PADDING_LEFT,
          pos_y = PADDING_TOP;

        const AR = img.width / img.height;
        const SCREEN_AR = window.innerWidth / window.innerHeight;
        const is_img_landscape = AR > 1;
        const is_screen_landscape = SCREEN_AR > 1;
        if (is_img_landscape) {
          img.width = window.innerWidth - (PADDING_LEFT + PADDING_RIGHT);
          img.height = img.width / AR;
          pos_x = PADDING_LEFT;
          pos_y =
            PADDING_TOP +
            (window.innerHeight - (PADDING_TOP + PADDING_BOTTOM + img.height)) /
              2;
        } else {
          img.height = window.innerHeight - (PADDING_TOP + PADDING_BOTTOM);
          img.width = img.height * AR;
          pos_y = PADDING_TOP;
          pos_x =
            PADDING_LEFT +
            (window.innerWidth - (PADDING_LEFT + PADDING_RIGHT + img.width)) /
              2;
        }

        // console.log(`IMG_W: ${img.width}, IMG_H: ${img.height}`);
        ctx.drawImage(img, pos_x, pos_y, img.width, img.height);
      };
      img.src = event.target.result;
    };
    reader.readAsDataURL(image_file);
  }
}
