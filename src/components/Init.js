import Whiteboard from "./Whiteboard";

export default function init() {


  console.log("Initializing default configurations..");
  window.whiteboard = new Whiteboard();
  whiteboard.clear_white_board();

  const update_color_in_color_selector = () => {
    // Update the color in color selector.
    const selected = document.querySelector("div.color-picker  span.selected");
    selected.style.backgroundColor =
      Whiteboard.supported_pen_colors[whiteboard.current_pen_color];
  };

  const update_color_selctor_circle_size = () => {
    if (!whiteboard.is_eraser_enabled()) {
      document.getElementById("currentColorCircle").style.width = `${
        whiteboard.current_pen_size * 3
      }px`;
      document.getElementById("currentColorCircle").style.height = `${
        whiteboard.current_pen_size * 3
      }px`;
    }
  };

  function click_handler(event) {
    //console.log(event);
    if (event.ctrlKey) {
      whiteboard.change_marker_color();
      update_color_in_color_selector();
    } else {
      whiteboard.draw(event);
    }
  }

  function dblclick_handler() {
    console.log("Double click handler!");
  }

  function contextmenu_handler(e) {
    e.preventDefault();

    whiteboard.draw(e);
    console.log("Context Menu Handler");
  }

  window.toggle_eraser = () => {
    if (whiteboard.is_eraser_enabled()) {
      whiteboard.change_marker_color(
        Whiteboard.supported_pen_colors[whiteboard.current_pen_color]
      );
    } else {
      whiteboard.change_marker_color(PEN_COLOR.ERASE);
    }
  }

  window.download_whiteboard_to_png = () => {
    const link = document.createElement("a");
    link.download = "whiteboard.png";
    link.href = document.getElementById("whiteboard").toDataURL();
    link.click();
  }

  whiteboard.board.ontouchmove = whiteboard.draw;
  whiteboard.board.ontouchstart = whiteboard.pen_down;
  whiteboard.board.ontouchend = whiteboard.pen_up;
  whiteboard.board.onmousemove = whiteboard.draw;
  whiteboard.board.onmousedown = whiteboard.pen_down;
  whiteboard.board.onmouseup = whiteboard.pen_up;
  whiteboard.board.onclick = click_handler;
  whiteboard.board.ondblclick = dblclick_handler;
  whiteboard.board.oncontextmenu = contextmenu_handler;

  eraserButton.onclick = toggle_eraser;
  downloadWhiteboardButton.onclick = download_whiteboard_to_png;
  clearWhiteboardButton.onclick = () => {
    whiteboard.clear_white_board(false);
  };

  // CTRL + Mouse wheel
  whiteboard.board.addEventListener("wheel", (event) => {
    if (event.ctrlKey) {
      event.preventDefault();

      const is_wheel_up = event.deltaY < 0;
      const is_wheel_down = event.deltaY > 0;

      if (is_wheel_up) {
        whiteboard.increase_pen_size();
      } else if (is_wheel_down) {
        whiteboard.decrease_pen_size();
      }

      if (whiteboard.is_eraser_enabled()) {
        const eraserCircle = document.getElementById("eraserCircle");
        eraserCircle.style.height = `${whiteboard.current_eraser_size * 2}px`;
        eraserCircle.style.width = `${whiteboard.current_eraser_size * 2}px`;
      }

      console.log(whiteboard.current_pen_size);
      update_color_selctor_circle_size();
    }
  });

  // Eraser circle
  whiteboard.board.addEventListener("mousemove", (event) => {
    const eraserCircle = document.getElementById("eraserCircle");

    if (whiteboard.is_eraser_enabled()) {
      eraserCircle.style.display = "block";
      eraserCircle.style.left = `${
        event.clientX - whiteboard.current_eraser_size
      }px`;
      eraserCircle.style.top = `${
        event.clientY - whiteboard.current_eraser_size
      }px`;
      eraserCircle.style.height = `${whiteboard.current_eraser_size * 2}px`;
      eraserCircle.style.width = `${whiteboard.current_eraser_size * 2}px`;
    } else {
      eraserCircle.style.display = "none";
    }
  });

  document.addEventListener("keydown", (event) => {
    // CTRL + Z
    if (event.ctrlKey && event.key === "z") {
      whiteboard.undo_stroke();
    }

    // CTRL + Y
    if (event.ctrlKey && event.key === "y") {
      whiteboard.redo_stroke();
    }

    // CTRL + space
    if (event.ctrlKey && event.key === " ") {
      whiteboard.clear_white_board();
    }
  });

  window.change_pen_color = (color) => {
    whiteboard.change_marker_color(color);
    update_color_in_color_selector();
  };
}
