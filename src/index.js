import './styles/global.css';

import './components/Globals';
import init from './components/Init';

window.onload = () => {
  init();
}