# likaa.com
A No-Nonsense Online Whiteboard with handy shortcuts to quickly bring your thought to sketch.


## Discord Server
https://discord.gg/6dK689Sf


## Product Roadmap
https://docs.google.com/spreadsheets/d/1eBUUc_TF35MfPrmlnATD54rMoyMVkk_FEZpjxxv10-g/edit?usp=sharing


## Figma Design System
https://www.figma.com/files/project/47329826/Likhaa.com?fuid=1066470952012987621


## References

- A simplest whiteboard drawing code in just few lines for reference
https://www.sololearn.com/en/compiler-playground/WxJlEOqLV5IL

